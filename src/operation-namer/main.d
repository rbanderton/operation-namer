enum string DEFAULT_INPUT_FILE = "words.txt";

int main(string[] argv)
{
	import std.array : array;
	import std.file : isFile;
	import std.random : Random, uniform, unpredictableSeed;
	import std.stdio;

    auto filename = argv.length > 1? argv[1] : DEFAULT_INPUT_FILE;

	stdout.writef("Loading words from %s...", filename);

	if(!isFile(filename))
	{
		stderr.writeln("Could not load word list");
		return 1;
	}

	auto words = File(filename).byLineCopy().array();
	
	stdout.writefln("done - %d words found.", words.length);

	auto lastWord = words.length - 1;

	auto gen = Random(unpredictableSeed);
	auto primaryIdx = uniform(0, lastWord, gen);
	auto secondaryIdx = uniform(0, lastWord, gen);

	auto result = "Operation " ~ words[primaryIdx] ~ " " ~ words[secondaryIdx];

	stdout.writeln();
	stdout.writefln(result);
	stdout.writeln();

	return 0;
}
